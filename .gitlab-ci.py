from gcip import (
    Image,
    PagesJob,
    Pipeline,
    PredefinedVariables,
)
from gcip.addons.python import jobs as python
from gcip.addons.gitlab.jobs import pages as gitlab_pages
from gcip.addons.container.sequences import build

pipeline = Pipeline()
pipeline.initialize_image("python:3.7-slim")

pipeline.add_children(
    python.isort(),
    python.flake8(),
    python.pytest(),
    python.mypy("gcip"),
    # pydoc3 tries to resolve all Python objects and their attributes.
    # This includes PredefinedVariables class which are not necessarily set in every pipeline,
    # like CI_DEBUG_TRACE or CI_DEPLOY_PASSWORD . Resolving those variables lead to a KeyError.
    # When CI is unset, then PredefinedVariables behave different, returns mock values for all
    # other unset variables. That makes pdoc3 running without stumbling over those Key errors.
    gitlab_pages.pdoc3(module="gcip", output_path="/api").prepend_scripts("unset CI"),
    gitlab_pages.asciidoctor(source="docs/index.adoc", out_file="/index.html", job_name="pages_index"),
    gitlab_pages.asciidoctor(source="docs/user/index.adoc", out_file="/user/index.html", job_name="pages_user_index"),
    python.bdist_wheel(),
)

# gitlabci-local only works with 'sh' as kaniko and crane entrypoint
kaniko_image = None
crane_image = None
if PredefinedVariables.CI_COMMIT_REF_SLUG == "gitlab-local-sh":
    kaniko_image = Image("gcr.io/kaniko-project/executor:debug", entrypoint=["sh"])
    crane_image = Image("gcr.io/go-containerregistry/crane:debug", entrypoint=["sh"])

pipeline.add_children(
    build.FullContainerSequence(
        image_name="thomass/gcip",
        kaniko_kwargs={"kaniko_image": kaniko_image},
        crane_kwargs={"crane_image": crane_image},
        do_crane_push=PredefinedVariables.CI_COMMIT_TAG is not None or PredefinedVariables.CI_COMMIT_REF_NAME == "main",
    ),
    name="gcip",
)

if PredefinedVariables.CI_COMMIT_TAG:
    pipeline.add_children(
        python.evaluate_git_tag_pep440_conformity(),
        python.twine_upload(),
    )

pipeline.add_children(PagesJob())
pipeline.write_yaml()
