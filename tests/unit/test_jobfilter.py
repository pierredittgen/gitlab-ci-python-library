import pytest

from gcip import Job, JobFilter
from gcip.lib import rules
from gcip.core.need import Need
from gcip.core.rule import Rule
from gcip.core.cache import Cache
from gcip.core.sequence import Sequence
from gcip.core.artifacts import Artifacts
from gcip.addons.container.images import PredefinedImages


@pytest.fixture
def goodjob(badjob) -> Job:
    job = Job(
        name="five100sixteen",
        stage="nine100twelve",
        script=["five9ten", "seven8nine"],
    )
    job.set_image(PredefinedImages.GCIP)
    job.add_variables(FOO="one2three", BAR="four5six")
    job.add_tags("five5one", "eight3three")
    job.append_rules(rules.on_main(), rules.on_tags())
    return job


@pytest.fixture
def badjob() -> Job:
    job = Job(
        name="obey",
        stage="obey",
        script=["five9ten", "obey"],
    )
    job.set_image(PredefinedImages.DIVE)
    job.add_variables(FOO="zero4eight", BUFF="four5six")
    job.add_tags("one0two", "eight3three")
    job.append_rules(rules.on_master(), rules.on_tags())
    return job


def test_namefilter(goodjob, badjob):
    print("foobar")
    filter = JobFilter(name="five\\d{3}sixteen-\\w*")
    assert filter.match(goodjob)
    assert not filter.match(badjob)


def test_stagefilter(goodjob, badjob):
    filter = JobFilter(stage="nine\\d{3}\\w+ve")
    assert filter.match(goodjob)
    assert not filter.match(badjob)


def test_scriptfilter(goodjob, badjob):
    filter1 = JobFilter(script="five\\dten")
    assert filter1.match(goodjob)
    assert filter1.match(badjob)

    filter2 = JobFilter(script=["seven\\w+$", "five\\dten"])
    assert filter2.match(goodjob)
    assert not filter2.match(badjob)


def test_imagefilter(goodjob, badjob):
    filter1 = JobFilter(image="^.*/gcip:.*$")
    assert filter1.match(goodjob)
    assert not filter1.match(badjob)

    filter2 = JobFilter(image=PredefinedImages.GCIP)
    assert filter2.match(goodjob)
    assert not filter2.match(badjob)


def test_variablesfilter(goodjob, badjob):
    filter1 = JobFilter(variables={"FOO": "on.*ee"})
    assert filter1.match(goodjob)
    assert not filter1.match(badjob)

    filter2 = JobFilter(variables={"BAR": "four5six", "FOO": "on.*ee"})
    assert filter2.match(goodjob)
    assert not filter2.match(badjob)

    filter3 = JobFilter(variables={"FOO": "zero4eight", "BAR": "four5six"})
    assert not filter3.match(badjob)


def test_tagsfilter(goodjob, badjob):
    filter1 = JobFilter(tags="five5one")
    assert filter1.match(goodjob)
    assert not filter1.match(badjob)

    filter2 = JobFilter(tags=["eight3three", "five5one"])
    assert filter2.match(goodjob)
    assert not filter2.match(badjob)


def test_rulesfilter(goodjob, badjob):
    filter1 = JobFilter(rules=rules.on_main())
    assert filter1.match(goodjob)
    assert not filter1.match(badjob)

    filter2 = JobFilter(rules=Rule(if_statement='$CI_COMMIT_BRANCH == "main"'))
    assert filter2.match(goodjob)
    assert not filter2.match(badjob)

    filter3 = JobFilter(rules=[rules.on_tags(), rules.on_main()])
    assert filter3.match(goodjob)
    assert not filter3.match(badjob)


def test_needsjobfilter(goodjob, badjob):
    goodjob.add_needs(badjob)

    filter1 = JobFilter(needs=badjob)
    assert filter1.match(goodjob)

    filter2 = JobFilter(needs="obey-obey")
    assert filter2.match(goodjob)


def test_needssequencefilter(goodjob, badjob):
    sequence = Sequence().add_children(badjob)
    goodjob.add_needs(sequence)

    filter1 = JobFilter(needs=sequence)
    assert filter1.match(goodjob)

    filter2 = JobFilter(needs="obey-obey")
    assert filter2.match(goodjob)


def test_needsfilter(goodjob):
    goodjob.add_needs(Need(job="eight7six", project="otherproj"))

    filter1 = JobFilter(needs=Need(job="eight7six", project="otherproj"))
    assert filter1.match(goodjob)

    filter2 = JobFilter(needs="ei.*7si\\w$")
    assert filter2.match(goodjob)


def test_artifactsfilter(goodjob, badjob):
    goodjob.artifacts.add_paths("foo", "bar")
    goodjob.artifacts.public = False

    badjob.artifacts.add_paths("foo", "bar")

    artifacts = Artifacts("foo", "bar", public=False)
    filter1 = JobFilter(artifacts=artifacts)
    assert filter1.match(goodjob)
    assert not filter1.match(badjob)

    filter2 = JobFilter(artifacts=["b\\wr", "\\woo$"])
    assert filter2.match(goodjob)

    goodjob.artifacts.add_paths("trio")
    assert filter2.match(goodjob)

    filter3 = JobFilter(artifacts=["b\\wr", "\\woo$", "\\wrio"])
    assert filter3.match(goodjob)
    assert not filter3.match(badjob)


def test_cache(goodjob, badjob):
    goodjob.set_cache(Cache(paths=["foo", "bar"], untracked=False))
    badjob.set_cache(Cache(paths=["foo", "bar"]))

    filter1 = JobFilter(cache=Cache(paths=["foo", "bar"], untracked=False))
    assert filter1.match(goodjob)
    assert not filter1.match(badjob)

    filter2 = JobFilter(cache=["\\./b\\w\\w", ".*\\woo"])
    assert filter2.match(goodjob)
