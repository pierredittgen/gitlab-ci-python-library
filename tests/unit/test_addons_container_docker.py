from gcip import Pipeline
from tests import conftest
from gcip.addons.container.jobs import docker


def test_default_docker_jobs():
    pipeline = Pipeline()

    pipeline.add_children(
        docker.Build(repository="myspace/myimage"),
        docker.Push(image="myspace/myimage"),
    )

    conftest.check(pipeline.render())
