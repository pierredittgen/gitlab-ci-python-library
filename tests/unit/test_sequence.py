from gcip import Job, Sequence


def test_name_population():
    job = Job(name="a", stage="b", script="foobar")
    sequence1 = Sequence().add_children(job, name="c", stage="d")
    sequence2 = Sequence().add_children(sequence1, name="e", stage="f")
    assert sequence2.populated_jobs[0].name == "e-f-c-d-a-b"
